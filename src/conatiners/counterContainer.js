import {connect} from "react-redux"
import Home from "../component/Home"
import {incrementAction,decrementAction} from "./actions/counterAction"

const mapstatetoprops=(state)=>{
    return{
        count: state.count
    }
}

const mapdispatchtoprops=(dispatch)=>{
    return{
        Increment:()=>{dispatch(incrementAction())},
        Decrement:()=>{dispatch(decrementAction())}
    }
}

export default connect(mapdispatchtoprops,mapstatetoprops)(Home)