import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';
import counterReducer from './reducers/counterReducer'
import { combineReducers} from 'redux'
import { Provider } from 'react-redux';
import Home from './component/Home';
import {configureStore} from "@reduxjs/toolkit"

const root = ReactDOM.createRoot(document.getElementById('root'));
const store= configureStore({
  reducer: combineReducers({counterReducer}),
  devTools: "true"
})
//const store= createStore(counterReducer)
root.render(
  <Provider store={store}>
      <Home/>
  </Provider>    
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
