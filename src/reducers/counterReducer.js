// import { incrementAction,decrementAction } from "../actions/counterAction";
const initialState={
    count:0
}
const counterReducer=(state=initialState,action)=>{
    switch(action.type){
        case "Increment": return{ count: state.count + 1}//action.payload}
        case "Decrement" : return{ count: state.count- 1} // action.payload}
        default: return{state}
    }
}

export default counterReducer;